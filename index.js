const fetch = require('node-fetch');
const Afplay = require('afplay');
const chalk = require('chalk');

const player = new Afplay;

const queryLocations = () => {
  const query = `query SearchPharmaciesNearPointWithCovidVaccineAvailability($latitude: Float!, $longitude: Float!, $radius: Int! = 10) {
    searchPharmaciesNearPoint(latitude: $latitude, longitude: $longitude, radius: $radius) {
      distance
      location {
        locationId
        name
        nickname
        phoneNumber
        isCovidVaccineAvailable
        covidVaccineEligibilityTerms
      }
    }
  }`;
  const latitude = 40.813616;
  const longitude = -96.7025955;
  const radius = 60;
  
  fetch('https://www.hy-vee.com/my-pharmacy/api/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': '*/*',
    },
    body: JSON.stringify({
      query,
      variables: {
        latitude,
        longitude,
        radius
      }
    })
  })
    .then(r => r.json())
    .then((data) => {
      const results = data.data.searchPharmaciesNearPoint;
      const covidAvailable = results.filter((result) => result.location.isCovidVaccineAvailable);

      console.log(chalk.bold.blueBright('Checking...'));

      if (covidAvailable.length) {
        console.log(chalk.bold.redBright(`Found ${covidAvailable.length} locations!`));
        covidAvailable.forEach((available) => {
          console.table(available.location);
        });
        player.play('/System/Library/Sounds/Sosumi.aiff')
          .then(() => {
            player.play('/System/Library/Sounds/Sosumi.aiff')
              .then(() => {
                console.log('done');
              });
          });
        }
    });
}

setInterval(queryLocations, 5000);